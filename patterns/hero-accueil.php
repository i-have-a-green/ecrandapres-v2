<?php
/**
 * Title: Hero page d'accueil avec logo + video
 * Slug: ihag/hero-accueil
 * Categories:  global
 */

?>

<!-- wp:group {"align":"wide","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="margin-top:var(--wp--preset--spacing--50);margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%"><!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|80"}}},"layout":{"type":"flex","orientation":"vertical","justifyContent":"left","flexWrap":"wrap"}} -->
<div class="wp-block-group" style="padding-right:var(--wp--preset--spacing--80)"><!-- wp:image {"align":"left","id":61,"width":618,"height":188,"sizeSlug":"large","linkDestination":"none","className":"is-style-default"} -->
<figure class="wp-block-image alignleft size-large is-resized is-style-default image-is-left"><img src="http://lecran-dapres.local/wp-content/uploads/2022/10/1_Logo_horizontal_symb-couleur-typo-blanc-1024x312.png" alt="" class="wp-image-61" width="618" height="188"/></figure>
<!-- /wp:image -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"0","bottom":"0","right":"0rem","left":"0"}}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"left"}} -->
<div class="wp-block-group" style="padding-top:0;padding-right:0rem;padding-bottom:0;padding-left:0"><!-- wp:heading {"level":1,"style":{"typography":{"textTransform":"none","fontStyle":"normal","fontWeight":"400"},"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}}},"fontSize":"large"} -->
<h1 class="has-large-font-size" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;font-style:normal;font-weight:400;text-transform:none">Une démarche inédite pour intégrer <br> les enjeux sociaux et environnementaux dans les fictions</h1>
<!-- /wp:heading --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%"><!-- wp:embed {"url":"https://www.youtube.com/watch?v=cv2j5LoU1uU","type":"video","providerNameSlug":"youtube","allowResponsive":false,"responsive":true,"className":""} -->
<figure class="wp-block-embed is-type-video is-provider-youtube wp-block-embed-youtube"><div class="wp-block-embed__wrapper">
https://www.youtube.com/watch?v=cv2j5LoU1uU
</div></figure>
<!-- /wp:embed --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
