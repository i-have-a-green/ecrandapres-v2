<?php
/**
 * Title: Block gauche avec partners + bouton
 * Slug: ihag/block-gauche
 * Categories:  global
 */

?>

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"2.5rem","right":"2.5rem","bottom":"2.5rem","left":"2.5rem"}}},"className":"block-left-screen","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull block-left-screen" style="padding-top:2.5rem;padding-right:2.5rem;padding-bottom:2.5rem;padding-left:2.5rem"><!-- wp:group {"align":"wide","layout":{"type":"constrained","justifyContent":"left"}} -->
<div class="wp-block-group alignwide"><!-- wp:heading -->
<h2>Ils font partie de L'écran d'après</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"}} -->

<?php 
    $args = array( 'post_type' => 'partner', 'posts_per_page' => 4 );
    $the_query = new WP_Query( $args ); 
?>
<?php if ( $the_query->have_posts() ) : ?>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <div class="wp-block-group"><!-- wp:image {"id":224,"sizeSlug":"thumbnail","linkDestination":"none"} -->
        <figure class="wp-block-image size-thumbnail"><?php the_post_thumbnail('thumbnail'); ?></figure>
        <!-- /wp:image -->

    <?php endwhile;
    wp_reset_postdata(); ?>
    <?php else:  ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<!-- /wp:group -->

<!-- wp:buttons {"className":"button-bottom-div","layout":{"type":"flex","justifyContent":"right","orientation":"horizontal"},"fontSize":"medium"} -->
<div class="wp-block-buttons has-custom-font-size button-bottom-div has-medium-font-size"><!-- wp:button {"backgroundColor":"quaternary"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-quaternary-background-color has-background wp-element-button" href="/partenaires/">Découvrir nos partenaires</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"37px"} -->
<div style="height:37px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group -->
