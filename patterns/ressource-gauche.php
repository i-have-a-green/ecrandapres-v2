<?php
/**
 * Title: block ressource gauche
 * Slug: ihag/block-ressource-gauche
 * Categories:  global
 */

?>

<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:group {"align":"full","className":"ressource-gauche-container-title","layout":{"type":"constrained","justifyContent":"left"}} -->
<div class="wp-block-group alignfull ressource-gauche-container-title"><!-- wp:heading {"textAlign":"right","align":"wide","style":{"color":{"gradient":"linear-gradient(135deg,rgb(111,211,108) 0%,rgb(55,157,226) 100%)"}},"textColor":"primary"} -->
<h2 class="alignwide has-text-align-right has-primary-color has-text-color has-background" style="background:linear-gradient(135deg,rgb(111,211,108) 0%,rgb(55,157,226) 100%)">Les personnages</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","className":"ressource-gauche-container","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull ressource-gauche-container"><!-- wp:heading {"level":3,"align":"wide","textColor":"quinary","fontSize":"large"} -->
<h3 class="alignwide has-quinary-color has-text-color has-large-font-size">Les chiffre clé</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","className":"container-ressources-card","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide container-ressources-card"><!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title","fontSize":"large"} -->
<h4 class="has-large-font-size">Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource --></div>
<!-- /wp:group -->

<!-- wp:heading {"level":3,"align":"wide","textColor":"quinary","fontSize":"large"} -->
<h3 class="alignwide has-quinary-color has-text-color has-large-font-size">Etudes et rapports</h3>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide","className":"container-ressources-card","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide container-ressources-card"><!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource -->

<!-- wp:acf/ressource {"name":"acf/ressource","mode":"auto"} -->
<!-- wp:heading {"level":4,"placeholder":"Title"} -->
<h4>Femme :</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>39% de l'ensemble des personnages</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>38% des personnages principaux</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test","className":"is-style-link-style"} -->
<div class="wp-block-button is-style-link-style"><a class="wp-block-button__link wp-element-button">Etude cinégalité 2022</a></div>
<!-- /wp:button -->
<!-- /wp:acf/ressource --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
