<?php
/**
 * Title: Block gauche avec text + bouton
 * Slug: ihag/block-gauche
 * Categories:  global
 */

?>

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"0","right":"0","bottom":"0","left":"0"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull" style="padding-top:0;padding-right:0;padding-bottom:0;padding-left:0"><!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"2.5rem","right":"2.5rem","bottom":"2.5rem","left":"2.5rem"}}},"className":"block-left-screen","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull block-left-screen" style="padding-top:2.5rem;padding-right:2.5rem;padding-bottom:2.5rem;padding-left:2.5rem"><!-- wp:group {"align":"wide","layout":{"type":"constrained","justifyContent":"left"}} -->
<div class="wp-block-group alignwide"><!-- wp:heading -->
<h2>Découvrez la grille de questionnement</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla elit ipsum, facilisis dictum ipsum ac, lobortis viverra metus.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"className":"button-bottom-div","layout":{"type":"flex","justifyContent":"right","orientation":"horizontal"},"fontSize":"medium"} -->
<div class="wp-block-buttons has-custom-font-size button-bottom-div has-medium-font-size"><!-- wp:button {"backgroundColor":"quaternary"} -->
<div class="wp-block-button"><a class="wp-block-button__link has-quaternary-background-color has-background wp-element-button">Commencer à me questionner</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"37px"} -->
<div style="height:37px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group -->
