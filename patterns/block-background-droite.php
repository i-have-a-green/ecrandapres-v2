<?php
/**
 * Title: Block droite background avec text + bouton
 * Slug: ihag/block-background-droit
 * Categories:  global
 */

?>


<!-- wp:group {"align":"full","style":{"color":{"gradient":"linear-gradient(90deg,rgb(111,211,108) 0%,rgb(55,157,226) 99%)"},"spacing":{"padding":{"top":"var:preset|spacing|70","right":"var:preset|spacing|70","bottom":"var:preset|spacing|70","left":"var:preset|spacing|70"}}},"className":"block-right-screen-background","layout":{"type":"constrained","justifyContent":"center"}} -->
<div class="wp-block-group alignfull block-right-screen-background has-background" style="background:linear-gradient(90deg,rgb(111,211,108) 0%,rgb(55,157,226) 99%);padding-top:var(--wp--preset--spacing--70);padding-right:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70);padding-left:var(--wp--preset--spacing--70)"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"16rem"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:heading {"textColor":"primary"} -->
<h2 class="has-primary-color has-text-color">Le centre de ressources</h2>
<!-- /wp:heading -->

<!-- wp:buttons {"align":"wide"} -->
<div class="wp-block-buttons alignwide"><!-- wp:button {"className":"is-style-background-button is-style-no-background-button"} -->
<div class="wp-block-button is-style-background-button is-style-no-background-button"><a class="wp-block-button__link wp-element-button">Voir toutes les ressources</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->

<!-- wp:buttons {"className":"has-custom-font-size align-buttons","layout":{"type":"flex","orientation":"horizontal","justifyContent":"center","verticalAlignment":"center"}} -->
<div class="wp-block-buttons has-custom-font-size align-buttons"><!-- wp:button {"style":{"typography":{"fontSize":"1.3rem"}},"className":"is-style-no-background-button"} -->
<div class="wp-block-button has-custom-font-size is-style-no-background-button" style="font-size:1.3rem"><a class="wp-block-button__link wp-element-button">Découvrir les chiffres clé</a></div>
<!-- /wp:button -->

<!-- wp:button {"style":{"typography":{"fontSize":"1.3rem"}},"className":"is-style-no-background-button"} -->
<div class="wp-block-button has-custom-font-size is-style-no-background-button" style="font-size:1.3rem"><a class="wp-block-button__link wp-element-button">Découvrir les organismes et experts</a></div>
<!-- /wp:button -->

<!-- wp:button {"style":{"typography":{"fontSize":"1.3rem"}},"className":"is-style-no-background-button"} -->
<div class="wp-block-button has-custom-font-size is-style-no-background-button" style="font-size:1.3rem"><a class="wp-block-button__link wp-element-button">Découvrir les études et rapports</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
