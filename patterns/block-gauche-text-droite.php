<?php
/**
 * Title: block gauche avec background + text à droite
 * Slug: ihag/block-background-gauche-text
 * Categories:  global
 */

?>

<!-- wp:group {"align":"full","layout":{"type":"flex","flexWrap":"wrap","justifyContent":"left","orientation":"horizontal"}} -->
<div class="wp-block-group alignfull"><!-- wp:group {"style":{"spacing":{"padding":{"top":"1rem","right":"1rem","bottom":"1rem","left":"1rem"}},"color":{"gradient":"linear-gradient(225deg,rgb(55,157,226) 0%,rgb(111,211,108) 100%)"},"border":{"radius":"40px"}},"textColor":"primary","className":"block-left-screen-background","layout":{"type":"constrained"}} -->
<div class="wp-block-group block-left-screen-background has-primary-color has-text-color has-background" style="border-radius:40px;background:linear-gradient(225deg,rgb(55,157,226) 0%,rgb(111,211,108) 100%);padding-top:1rem;padding-right:1rem;padding-bottom:1rem;padding-left:1rem"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":224,"sizeSlug":"medium","linkDestination":"none"} -->
<figure class="wp-block-image size-medium"><img src="http://lecran-dapres.local/wp-content/uploads/2022/10/test-photo-profil-300x240.jpeg" alt="" class="wp-image-224"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"style":{"spacing":{"padding":{"top":"var:preset|spacing|20","right":"0","bottom":"var:preset|spacing|20","left":"0"}}},"fontSize":"large"} -->
<h2 class="has-large-font-size" style="padding-top:var(--wp--preset--spacing--20);padding-right:0;padding-bottom:var(--wp--preset--spacing--20);padding-left:0">Point de focus</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque malesuada mollis sodales. Nunc volutpat in lectus sit amet tincidunt. Morbi dictum ac massa sed vehicula. Nullam vitae metus non nibh fringilla venenatis in eu velit. Proin imperdiet congue nunc vel congue. Pellentesque in viverra enim, nec consectetur justo.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"level":3} -->
<h3>Nouveaux Récits / FDR / Media Club / GTM</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Lorem ipsum dolor sit amet. Cum velit repellendus sed officiis laudantium est voluptates laborum et nostrum eligendi ut adipisci aliquam ab asperiores praesentium. Qui ipsum atque sed deleniti voluptate et saepe voluptatibus sed eligendi veniam aut possimus illo. Cum explicabo obcaecati ut repudiandae iste 33 adipisci rerum et amet quae ad magnam porro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Eos cupiditate sunt qui quisquam voluptatibus qui quia asperiores! Id amet laboriosam cum voluptatum tempora hic aliquam dolorem.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Et explicabo nihil qui accusamus quibusdam vel distinctio excepturi At odio fugiat in repellendus odit non dolores veritatis qui aspernatur officia. Perspiciatis dignissimos a itaque nihil rem consequatur similique eum unde tempora eos soluta inventore! Sed itaque molestiae et consectetur rerum et minus velit ab repellat repellendus ut rerum numquam.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Est autem quia eos laborum reprehenderit in debitis quia. Quo neque atque aut repellat reprehenderit est omnis mollitia aut unde corrupti aut commodi deserunt eum necessitatibus voluptas eos dolorem galisum. Et velit doloremque sed esse quibusdam cum quae fugiat a Quis molestiae.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Eos cupiditate sunt qui quisquam voluptatibus qui quia asperiores! Id amet laboriosam cum voluptatum tempora hic aliquam dolorem.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size">Et explicabo nihil qui accusamus quibusdam vel distinctio excepturi At odio fugiat in repellendus odit non dolores veritatis qui aspernatur officia. Perspiciatis dignissimos a itaque nihil rem consequatur similique eum unde tempora eos soluta inventore! Sed itaque molestiae et consectetur rerum et minus velit ab repellat repellendus ut rerum numquam.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
