<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Armando
 * @since 1.0.0
 */

/**
 * The theme version.
 *
 * @since 1.0.0
 */



add_action('init', 'acf_init_blocs');
function acf_init_blocs()
{
        /*
        EXEMPLE STRUCTURE BLOC CUSTOM ACF :

        acf_register_block_type(
            array(
                'name'				    => 'Slug du bloc',
                'title'				    => __('Nom back office'),
                'description'		    => __('Description back office'),
                'render_template'	    => 'template-parts/block/le-block.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'enqueue_script'        => get_template_directory_uri() . '/js/le-block.js',
                'keywords'			    => array(
                                            '',
                                            '',
                                            ''
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        */

}

//insertion des blocks customs
add_action('init', 'register_acf_blocks');
function register_acf_blocks()
{
    register_block_type(__DIR__ . '/blocks/listing-article');
    register_block_type(__DIR__ . '/blocks/newsletter');
    register_block_type(__DIR__ . '/blocks/ressource-custom');
    register_block_type(__DIR__ . '/blocks/formulaire-contact');
    register_block_type(__DIR__ . '/blocks/bottom-single-nav');
    register_block_type(__DIR__ . '/blocks/modal-image');
    register_block_type(__DIR__ . '/blocks/hero');
    register_block_type(__DIR__ . '/blocks/partner');
    register_block_type(__DIR__ . '/blocks/ressource');
}

// insertion des cutoms post type
add_action('init', 'ihag_custom_post_property');
function ihag_custom_post_property(){
    register_post_type( 
        'partner',
        array(
            'labels' => array(
                'name' => __('partners', 'ihag'),
                'singular_name' => __('partner', 'ihag'),
            ),
            'menu_position' => 18,
            'menu_icon' => 'dashicons-portfolio',
            'hierarchical' => false,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'name'),
            'show_in_rest' => false,
            'has_archive' => true,
            'supports' => array('title','editor','thumbnail'),
        )
    );
}

//insertion des styles prédéfinis
add_action('init', 'register_block_styles');
function register_block_styles()
{
    register_block_style(
        'core/navigation-link',
        array(
            'name' => 'blue-background',
            'label' => __('Fond de couleur du lien de', 'ihag'),
        )
    );

    register_block_style(
        'core/button',
        array(
            'name' => 'no-background-button',
            'label' => __('Bouton sans background', 'ihag'),
        )
    );
}

//insertion du css
function ihag_scripts(){
    wp_enqueue_style( 'ihag_style', get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'ihag_scripts' );



/* add_action( 'wp_after_insert_post', function( $post_id, $post ) {
    if ( $post->post_type !== 'wp_global_styles' ) {
        // export theme.json
	$tree = WP_Theme_JSON_Resolver::get_theme_data( array(), array( 'with_supports' => false ) );
	// Merge with user data.
	$tree->merge( WP_Theme_JSON_Resolver::get_user_data() );
    $theme = $tree->get_data();

    file_put_contents(__DIR__.'/theme.json',json_encode($theme));
} */
