<?php
/**
 * Block name: Listing article
 */

?>

<div class="container-article<?php echo ! empty( $block['align'] ) ? ' align'.sanitize_key( $block['align'] ):''; ?>">

    <?php
    global $post;
            $args = array(
                'post_type'         => 'post',
                'post_status'       => 'publish',
                'posts_per_page'    => 3
            );
            
            $articles = get_posts($args);
            ?>

            <div class="list-articles">

                <?php
                if($articles) :
                    foreach($articles as $post) :
                        setup_postdata($post); ?>

                        <a href="<?php echo esc_url(get_permalink()); ?>" class="link-card-blog">
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="post-thumbnail">
                                    <?php
                                        the_post_thumbnail('medium');
                                    ?>
                                </div>
                                <div class="content">
                                    <header>
                                        <?php
                                            the_title('<h3 class="entry-title">', '</h3>');
                                        ?>
                                    </header>
                                    <!-- <?php the_excerpt();?> -->
                                </div>
                            </article><!-- #post-<?php the_ID(); ?> -->
                        </a>

                        <!-- <?php the_content(); ?> -->

                    <?php endforeach; wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        <p class="has-text-align-right">
            <a href="<?php echo get_post_type_archive_link('post');?>" class="button">
                <?php _e('Découvrir toutes nos actualités', 'ihag');?>
            </a>
        </p>
</div>
