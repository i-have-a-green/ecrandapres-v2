<?php
/**
 * Block name : Ressource
 */

$template = array(
    array( 'core/heading', array(
        'placeholder' => 'Title',
        'level' => 4,
    ) ),
    array( 'core/paragraph', array(
        'placeholder' => 'corps',
    ) ),
    array( 'core/button', array(
        'placeholder' => 'test',
        )
    ),
);

?>


<div class="ressource-card alignfull">
    <?php echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" />'; ?>
</div>
