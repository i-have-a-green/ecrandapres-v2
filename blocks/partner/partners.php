<?php
/**
 * Block name : partner
 */

?>

<div class="partners-container alignwide">
    <?php 
        $args = array( 'post_type' => 'partner', 'posts_per_page' => 10 );
        $the_query = new WP_Query( $args ); 
    ?>
    <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post();
            get_template_part( 'templates/partner', 'search' );
        endwhile;
        wp_reset_postdata(); ?>
        <?php else:  ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
</div>
